/*
   Copyright(C) 2019 techfreak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

extern crate reqwest;
use serde::de::DeserializeOwned;

pub mod api_get_vuln_scheme {
    use serde::{Deserialize,Serialize};

    #[derive(Debug,Serialize,Deserialize,Clone)]
    pub struct Adv {
        pub name: String,
        pub packages: Vec<String>,
        pub status: String,
        pub severity: String,
        #[serde(rename = "type")]
        pub type_field: String,
        pub affected: String,
        pub ticket: Option<String>,
        pub issues: Vec<String>
    }
   }

/// Get json response of the vulnerable packages endpoint.
#[allow(dead_code)]
pub async fn get_vuln_packages() ->Result<Vec<api_get_vuln_scheme::Adv>,reqwest::Error> 
{
    Ok(create_api_call::<Vec<api_get_vuln_scheme::Adv>>("vulnerable").await?)
}

/// Get json response of the vulnerable packages endpoint.
#[allow(dead_code)]
pub async fn get_all_packages() ->Result<Vec<api_get_vuln_scheme::Adv>,reqwest::Error>
{
    Ok(create_api_call::<Vec<api_get_vuln_scheme::Adv>>("all").await?)
}


/// Makes a request to archlinux with the route as the first param and the additional params as the second param.
async fn create_api_call<T>(route: &str) -> Result<T,reqwest::Error> 
where T: DeserializeOwned + std::fmt::Debug
{ 

    let q = format!("https://security.archlinux.org/issues/{}.json",route);
    let req  = reqwest::get(&q)
        .await?
        .json::<T>()
        .await?;
    Ok(req)
}

