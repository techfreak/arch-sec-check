/*
   Copyright (C) 2019  techfreak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

mod lib;
extern crate tokio;
use std::env;
const GREEN : &str = "\x1b[0;36m";
const YELLOW :&str ="\x1b[0m\x1b[1;33m";
const RED :&str ="\x1b[1;31m";
const WHITE :&str ="\x1b[0m";
const BLUE :&str ="\x1b[1;34m";
use tokio::runtime::Runtime;
fn check_vuln() -> Result<(), Box <dyn std::error::Error>> {
    let mut current_thread = Runtime::new()?;
    let res =  &current_thread.block_on(lib::get_vuln_packages())?;
    check(res)?;
    Ok(())
}

fn check_all() -> Result<(), Box <dyn std::error::Error>> {
    let mut current_thread = Runtime::new()?;
    let res =  &current_thread.block_on(lib::get_all_packages())?;         
    check(res)?;   
    Ok(())
}

fn color_text(color:&str,text:&str) -> String{
    format!("{}{}{}",color,text,WHITE)
}

#[derive(Debug,Clone)]
struct Report{
    installed_version: String,
    description:String,
    dependency_of:String,
    adv:lib::api_get_vuln_scheme::Adv
}
async fn check_pkg(pkg:lib::api_get_vuln_scheme::Adv) -> Result<Option<Report>,Box<dyn std::error::Error>>{
    use tokio::process::Command;
    let pacinfo  = Command::new("sh")
        .arg("-c")
        .arg(format!("pacman -Q --info {} | grep -e Version  -e Description -e 'Required By' |  cut -d: -f2- |  awk 'NF'", pkg.packages[0]).trim())
        .output()
        .await?;
    let raw_res = String::from_utf8_lossy(&pacinfo.stdout).clone();
    if raw_res != ""{
        let installed_pkg : Vec<&str> = raw_res.trim().lines().collect();
        if pkg.affected == installed_pkg[0].trim(){
            return Ok(Some(Report{installed_version:installed_pkg[0].trim().to_string(),description:installed_pkg[1].trim().to_string(),dependency_of:installed_pkg[2].trim().to_string(),adv:pkg}));
        }
    }
    Ok(None)
}

fn check(pkgs:&[lib::api_get_vuln_scheme::Adv])-> Result<(),Box <dyn std::error::Error>>{
    let promises = pkgs.iter().map(|pkg|check_pkg(pkg.clone()));
    let mut current_thread = Runtime::new()?;
    let async_results = current_thread.block_on(futures::future::join_all(promises));
    let reports:Vec<_>   = async_results
        .iter()
        .filter(|report| { report.as_ref().is_ok() && !report.as_ref().unwrap().is_none()})
        .collect();
    if !reports.is_empty() {
        for report in reports{
            let report = report.as_ref().unwrap().clone().unwrap();
            let name = &report.adv.packages[0];
            let sec_page = format!("https://security.archlinux.org/package/{}",name.replace("\"",""));
            println!("[{}]",color_text(YELLOW,name));
            println!("{}: {}",color_text(BLUE,"Description"),color_text(GREEN,&report.description));
            println!("{}: {}",color_text(BLUE,"Installed version"),color_text(YELLOW,&report.installed_version));
            println!("{}: {}",color_text(BLUE,"Type"),color_text(RED,&report.adv.type_field));
            println!("{}: {}",color_text(BLUE,"Severity"),color_text(RED,&report.adv.severity));
            println!("{}",sec_page);
            println!("{}: {}",color_text(BLUE,"Required By"),color_text(BLUE,&report.dependency_of));
            println!();
        }
    } else {
        println!("{}",color_text(YELLOW,"Did not found any affected packages on your system."));
    }
    Ok(())
}

fn check_for_error(func:Result<(),Box <dyn std::error::Error>>){
    match func {
        Ok(_) => {},
        Err(_) => {println!("{red}An error occured and the database could not be reached. Please check the internet connection and try again. {reset}",red=RED,reset=WHITE);}      
    }
}

fn main() -> Result<(),Box<dyn std::error::Error>>  {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1  || args.len() > 2{
        check_for_error(check_vuln());
    }
    else if args[1] == "--full" {
        check_for_error(check_all()); 
    }
    else if args[1] == "--help" {
        println!("Version: {version} \n\
            {blue}Arch-sec-check {white}is a vulnerability scanner. Which compares the installed application version with the arch security database. By default the scanner uses the vulnerable database.\n\n\
            {green}Usage:{white} arch-sec-check [OPTION] \n\
            {green}--full{white} checks against the complete database. \n\
            ",blue=BLUE,white=WHITE,green=GREEN,version=env!("CARGO_PKG_VERSION"));
    }
    Ok(())
}
