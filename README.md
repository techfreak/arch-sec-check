# arch vulnerability scanner

**Arch-sec-check** is a vulnerability scanner. Which compares the installed application version with the a
rch security database. By default the scanner uses the vulnerable database.

![alt text](screenshot.png "Screenshot")

### Aur
You can install the package from aur [here](https://aur.archlinux.org/packages/arch-sec-check-git/).​           

### How to run it

Download the pre-built [binary](https://gitlab.com/techfreak/arch-sec-check/-/jobs/artifacts/master/download?job=build):


```shell
curl -L "https://gitlab.com/techfreak/arch-sec-check/-/jobs/artifacts/master/download?job=build" --output arch-sec-check.zip
```

Next unpack the archive:

``` shell 
unzip arch-sec-check.zip
```

Now run the application:

```shell
chmod +x arch-sec-check
./arch-sec-check
```



### How to build it from source

**Prerequisites**:

* [rust](https://www.rust-lang.org/tools/install)    

  

1. First git clone this repository in to your development directory:

```shell
git clone https://gitlab.com/techfreak/arch-sec-check
```

2. And run the command

```shell
cd arch-sec-check
cargo build --release
```

3. **Profit** !



### Acknowledgments

* [Reqwest](https://github.com/seanmonstar/reqwest)

* [tokio](https://github.com/tokio-rs/tokio)

* [serde](https://github.com/serde-rs/serde)

  

### Authors

* techfreak


### License

This project is licensed under the GPLv2 - see the [LICENSE.md](LICENSE.md) file for details

